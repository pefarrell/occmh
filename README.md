# Multigrid mesh hierarchies from CAD data in Firedrake

This project constructs mesh hierarchies in firedrake from CAD data
for use in multigrid solvers. For example,

    mh = OpenCascadeMeshHierarchy("tests/data/t_twist.step", mincoarseh=3, maxcoarseh=3, levels=4)

makes a multigrid hierarchy with 4 levels from the geometry given in the STEP file,
using mesh size h = 3 for the coarsest grid.

## Installation

Do

    sudo apt install liboce-*-dev

And then follow the build instructions for pythonocc-core from https://github.com/tpaviot/pythonocc-core/blob/master/INSTALL.md .

## Boundary conditions

In order to do the CAD projection, occmh needs to control the colouring of surface facets.
(Colour #1 must match up to the first CAD surface, colour #2 to the second, and so on.)
To visualise the colouring it generates, first make the hierarchy, then do

    scripts/colourmesh /path/to/coarse-mesh-made-by-occmh.msh colours.pvd
