from setuptools import setup

setup(name="occmh",
      version="0.0.1",
      description="OpenCascade mesh hierarchy",
      packages = ["occmh"],
      scripts = ["scripts/colourmesh"])
