import occmh
from firedrake import *

import pytest
import os


@pytest.fixture(scope='module', params=[("spanner.step", 8), ("cylinder.step", 20), ("t_twist.step", 3)])
def stepdata(request):
    (stepfile, h) = request.param
    curpath = os.path.dirname(os.path.realpath(__file__))
    return (os.path.join(curpath, "data", stepfile), h)


@pytest.fixture(scope='module', params=[1, 2])
def order(request):
    return request.param

def test_poisson(stepdata, order):
    (stepfile, h) = stepdata
    mh  = occmh.OpenCascadeMeshHierarchy(stepfile, mincoarseh=h, maxcoarseh=h, levels=3, order=order, cache=False, verbose=False)

    # Solve Poisson
    mesh = mh[-1]
    V = FunctionSpace(mesh, "CG", 1)
    u = Function(V)
    v = TestFunction(V)

    f = Constant(1)
    F = inner(grad(u), grad(v))*dx - inner(f, v)*dx
    bcs = DirichletBC(V, Constant(0), 1)

    params = {
        "mat_type": "aij",
        "snes_type": "ksponly",
        "ksp_type": "fgmres",
        "ksp_max_it": 20,
        "ksp_monitor_true_residual": None,
        "pc_type": "mg",
        "pc_mg_type" : "full",
        "mg_levels_ksp_type": "chebyshev",
        "mg_levels_pc_type": "sor",
        "mg_coarse_ksp_type": "preonly",
        "mg_coarse_pc_type": "lu",
        "mg_coarse_pc_factor_mat_solver_type": "mumps",
        }

    solve(F == 0, u, bcs, solver_parameters=params)
